import numpy as np
from typing import Union, Callable, Union, List, Optional
from pathlib import Path

array_like = Union[list, np.ndarray]
path_like = Union[str, Path]
