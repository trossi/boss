FROM python:3.7-slim-buster

RUN apt-get update && apt-get install \
    git \
    -yq --no-install-suggests --no-install-recommends --allow-downgrades --allow-remove-essential --allow-change-held-packages \
  && apt-get clean

RUN pip3 install --upgrade --no-cache-dir \
  pip \
  setuptools

RUN pip3 install --upgrade --no-cache-dir \
  twine \
  pytest \
  coverage \
  sphinx-rtd-theme \
  matplotlib \
  GPy

CMD /bin/bash
